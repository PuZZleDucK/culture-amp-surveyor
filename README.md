
# Surveyor

[![Build Status](https://semaphoreci.com/api/v1/puzzleduck/culture-amp-surveyor/branches/master/badge.svg)](https://semaphoreci.com/puzzleduck/culture-amp-surveyor)

This is my Surveyor solution for the Culture Amp coding test. I have included checklists/plans I might remove from actual work, but have included here for your analysis

The code for this project can be found on [BitBucket Surveyor](https://bitbucket.org/PuZZleDucK/culture-amp-surveyor) and continuous integration can be monitored at [Semaphore](https://semaphoreci.com/puzzleduck/culture-amp-surveyor)

# Setup and execute

To setup surveyor run:
```
git clone https://bitbucket.org/PuZZleDucK/culture-amp-surveyor
cd culture-amp-surveyor
bundle install
```

To to run the tests:
```
bundle exec rspec
```

To run a short demo:
```
bundle exec lib/surveyor.rb
```

# Assumptions
- To keep the application I have assumed that two responses submitted by a single user counts as two responses. In a real application there would probably be an option to update a previous response rather than generating a new one.


# Plan
- [x] get it running/setup and cleanup notes
- [x] setup repo/ci and include links
- [x] make tests pass
 - [x] 0 - positive responses (score > 3) - negative responses (score < 3)
 - [x] 1 - participation percentage
 - [x] 2 - basic object
 - [x] 3 - rated between 1 and 5 except 'free_text'
 - [x] 4 - object with array
 - [x] 5 - multi array
 - [x] 6 - the grand finale
- [x] review submission


# Notes

- [x] Strong adherence to the ruby-style-guide
- [x] Clean & simple Ruby code to make the tests pass
- [x] Code that will still work if we were to change the tests requirements slightly

- [x] fix rubocop issues
- [x] detailed instructions can be found in the README file

- [x] generated using the `bundle gem` command from the Bundler gem
- [x] an (imaginary) gem called "surveyor", which helps represent survey data within Ruby
- [x] The `spec` directory contains some tests

- [x] Tests `00` and `01` contain code within the tests files themselves. This is to make it easier for you to navigate between the code and the tests
- [x] You don't necessarily have to only write code within the methods we provided
- [x] Think of these methods more as guidelines than solid boundaries

- [x] Tests from `02` to `06` expect the code to be within the `lib` directory
- [x] There are some placeholder classes in the `lib` directory already to get you started

- [x] Only one test will run at the beginning
- [x] If you want to check your syntax, run this command: ```bundle exec rubocop```

- [x] feel free to email through any questions

## The Tests

- [x] keep focussed on fixing just one broken test at a time
- [x] your goal is to make as many of the tests pass as possible
- [x] You will be assessed based on how you make the tests pass
- [x] pay attention to the error messages closely in the test and try to interpret what they're asking for
- [x] Try to focus on one test at a time and making that pass
- [x] You do not have to make all the tests pass to submit

## Submitting the coding test

- [x] complete no later by Sunday 8 October, 2017
- [x] please submit via the unique link below this email
