#!/usr/bin/env ruby
require 'surveyor/answer'
require 'surveyor/question'
require 'surveyor/response'
require 'surveyor/survey'
require 'surveyor/version'

# The Surveyor module for handling survey data
module Surveyor
  puts 'You can use the Surveyor Module to:'

  puts '  * Construct Questions:'
  q1 = Question.new(title: 'Enjoy asking questions?', type: 'free_text')
  q2 = Question.new(title: 'Prefer to rate things?', type: 'rating')
  puts "     - #{q1}"
  puts "     - #{q2}"

  puts '  * Answer Questions:'
  a1 = Answer.new(question: q1, value: 'Prefer to test utf-8 compliance ☣ lol')
  a2 = Answer.new(question: q2, value: 5)
  puts "     - #{a1}"
  puts "     - #{a2}"

  puts '  * Record Responses:'
  r1 = Response.new(user: 'socrates@athens.gr')
  puts "     - #{r1}"
  r1.answers << Answer.new(question: q1, value: 'Yes, I have even been known to answer them too')
  puts "     - #{r1}"
  r1.answers << Answer.new(question: q2, value: 5)
  puts "     - #{r1}"

  puts '  * Conduct Surveys:'
  s1 = Survey.new(name: 'Ontological surveys, do they exist?')
  puts "     - #{s1}"
  s1.questions << q1
  s1.questions << q2
  puts "     - #{s1}"
  s1.responses << r1
  puts "     - #{s1}"
end
