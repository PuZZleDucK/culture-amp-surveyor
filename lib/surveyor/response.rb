module Surveyor
  # Class for handling response data
  class Response
    attr_reader :answers, :user

    def initialize(user:)
      @user = user
      @answers = []
    end

    def to_s
      "{Response: user='#{@user}' answers=#{@answers}}"
    end

    def inspect
      "{R='#{@answers}'}"
    end
  end
end
