module Surveyor
  # Class for handling survey data
  class Survey
    attr_accessor :name, :questions, :responses

    def initialize(info)
      @name = info[:name]
      @questions = []
      @responses = []
    end

    def participation_percentage(question)
      response_count = responses.count do |response|
        response.answers.any? do |answer|
          answer.question == question
        end
      end
      (response_count / responses.count.to_f * 100).round 2
    end

    def ratings(question)
      ratings = { 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0 }
      results = responses.flat_map(&:answers)
        .select { |answer| answer.question == question }
        .group_by(&:value)
        .map { |rating, answers| [rating, answers.count]}
        .to_h
      ratings.merge(results)
    end

    def to_s
      "{Survey: name='#{@name}' questions=#{@questions} responses=#{@responses}}"
    end
  end
end
