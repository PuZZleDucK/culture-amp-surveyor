module Surveyor
  # Class for handling question data
  class Question
    attr_reader :title, :type

    def initialize(title:, type:)
      @title = title
      @type = type
    end

    def to_s
      "{Question: title='#{@title}' type='#{@type}'}"
    end

    def inspect
      "{Q='#{@title}'}"
    end
  end
end
