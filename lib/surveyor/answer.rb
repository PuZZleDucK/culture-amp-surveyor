module Surveyor
  # Class for handling answer data
  class Answer
    attr_reader :question, :value

    def initialize(question:, value:)
      @question = question
      @value = value
    end

    def valid?
      return false unless @question
      return true if @question.type == 'free_text'
      (1..5).cover?(value)
    end

    def to_s
      "{Answer: question=#{@question} value='#{@value}'}"
    end

    def inspect
      "{A: value='#{@value}'}"
    end
  end
end
