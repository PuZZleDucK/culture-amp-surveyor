require 'spec_helper'

RSpec.describe '01: Questions and Answers' do
  class Response
    def self.answered?(responses, user, question)
      user_entry = responses.find { |entry| entry[:user] == user }
      user_entry[:answers][question] if user_entry
    end

    def self.answer_for_question_by_user(responses, question, user)
      user_entry = responses.find { |entry| entry[:user] == user }
      user_entry[:answers][question] if user_entry
    end

    def self.question_average(responses, question)
      answers = responses.map { |response| response.dig(:answers, question) }.compact
      (answers.sum / answers.count.to_f).round(2)
    end

    def self.question_participation_percentage(responses, question)
      answers = responses.map { |response| response.dig(:answers, question) }.compact
      (answers.count / responses.count.to_f * 100).round 2
    end

    def self.overall_participation_percentage(responses)
      respondents = responses.count { |response| response[:answers].any? }
      (respondents / responses.count.to_f * 100).round 2
    end
  end

  let(:responses) do
    [
      { user: 'alice@example.com', answers: { q1: 1, q2: 2, q3: 4 } },
      { user: 'bob@example.com', answers: { q1: 1, q2: 4, q3: 5 } },
      { user: 'frank@example.com', answers: { q3: 5 } },
      { user: 'claire@example.com', answers: {} },
      { user: 'ella@example.com', answers: {} },
      { user: 'sharon@example.com', answers: {} },
      { user: 'ryan@example.com', answers: {} }
    ]
  end

  let(:empty_responses) do
    []
  end

  context 'answered?' do
    it 'determines that question 1 has been answered by alice' do
      expect(Response).to be_answered(responses, 'alice@example.com', :q1)
    end

    it 'determines that question has not been answered by frank' do
      expect(Response).not_to be_answered(responses, 'frank@example.com', :q2)
    end

    it 'does not error out if a respondent is not in the survey' do
      expect(Response).not_to be_answered(responses, 'augustus@example.com', :q2)
    end

    it 'does not error out if a survey is empty' do
      expect(Response).not_to be_answered(empty_responses, 'augustus@example.com', :q2)
    end
  end

  context 'answer_for_question_by_user' do
    it 'gets the answer for question 1 by bob' do
      expect(Response.answer_for_question_by_user(responses, :q1, 'bob@example.com')).to eq(1)
    end

    it 'gets no answer for question 4 from frank' do
      expect(Response.answer_for_question_by_user(responses, :q4, 'frank@example.com')).to be_nil
    end

    it 'returns nil for people who have not responded' do
      expect(Response.answer_for_question_by_user(responses, :q4, 'claire@example.com'))
    end

    it 'does not error out if a survey is empty' do
      expect(Response.answer_for_question_by_user(empty_responses, :q4, 'claire@example.com'))
    end
  end

  context 'question_average' do
    it 'determines the average for question 1' do
      expect(Response.question_average(responses, :q1)).to eq(1)
    end

    it 'determines the average for question 3' do
      expect(Response.question_average(responses, :q3)).to eq(4.67)
    end

    it 'does not error if a survey is empty' do
      expect(Response.question_average(empty_responses, :q3).nan?).to eq(true)
    end
  end

  context 'question participation percentage' do
    it 'works out participation percentage for question 1' do
      expect(Response.question_participation_percentage(responses, :q1)).to eq(28.57)
    end

    it 'works out the participation percentage for question 3' do
      expect(Response.question_participation_percentage(responses, :q3)).to eq(42.86)
    end

    it 'works out the participation percentage for question 4' do
      expect(Response.question_participation_percentage(responses, :q4)).to eq(0)
    end

    it 'does not error if a survey is empty' do
      expect(Response.question_participation_percentage(empty_responses, :q4).nan?).to eq(true)
    end
  end

  context 'overall participation percentage' do
    it 'works out the overall participation percentage' do
      expect(Response.overall_participation_percentage(responses)).to eq(42.86)
    end

    it 'does not error if a survey is empty' do
      expect(Response.overall_participation_percentage(empty_responses).nan?).to eq(true)
    end
  end
end
