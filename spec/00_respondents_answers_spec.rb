require 'spec_helper'
require 'active_support'
require 'active_support/core_ext'

RSpec.describe '00: Respondent Answers' do
  class Response
    def self.count(responses)
      responses.count
    end

    def self.for(responses, user)
      responses.detect { |response| response[:user] == user }
    end

    def self.present?(responses, user)
      responses.any? { |response| response[:user] == user }
    end

    def self.positive(responses)
      responses.count { |response| response[:answer] > 3 }
    end

    def self.negative(responses)
      responses.count { |response| response[:answer] < 3 }
    end

    def self.average(responses)
      responses.sum { |response| response[:answer] } / responses.count.to_f
    end
  end

  let(:responses) do
    [
      { user: 'alice@example.com', answer: 1 },
      { user: 'bob@example.com', answer: 2 },
      { user: 'frank@example.com', answer: 3 },
      { user: 'claire@example.com', answer: 4 }
    ]
  end

  let(:empty_responses) do
    []
  end

  let(:duplicate_responses) do
    [
      { user: 'flip@flop.com', answer: 1 },
      { user: 'flip@flop.com', answer: 5 },
      { user: 'flip@flop.com', answer: 2 },
      { user: 'flip@flop.com', answer: 4 },
      { user: 'flip@flop.com', answer: 3 }
    ]
  end

  context 'count' do
    it 'counts the number of responses' do
      expect(Response.count(responses)).to eq(4)
    end

    it 'correctly count empty responses' do
      expect(Response.count(empty_responses)).to eq(0)
    end

    it 'correctly counts multiple responses from a user' do
      expect(Response.count(duplicate_responses)).to eq(5)
    end
  end

  context 'for' do
    it 'finds the response from frank@example.com' do
      response = Response.for(responses, 'frank@example.com')
      expect(response[:answer]).to eq(3)
    end

    it 'finds the response from bob@example.com' do
      response = Response.for(responses, 'bob@example.com')
      expect(response[:answer]).to eq(2)
    end

    it 'finds all the responses from flip@flop.com' do
      expect(Response.count(duplicate_responses.select { |x| x[:user] == 'flip@flop.com' })).to eq(5)
    end
  end

  context 'present?' do
    it 'frank@example.com\'s response is present' do
      expect(Response.present?(responses, 'frank@example.com')).to be true
    end

    it 'bill@example.com\'s response is not present' do
      expect(Response.present?(responses, 'bill@example.com')).to be false
    end
  end

  context 'positive' do
    it 'finds the positive responses (score > 3)' do
      expect(Response.positive(responses)).to eq(1)
    end

    it 'works with no responses' do
      expect(Response.positive(empty_responses)).to eq(0)
    end
  end

  context 'negative' do
    it 'finds the negative responses (score < 3)' do
      expect(Response.negative(responses)).to eq(2)
    end

    it 'works with no responses' do
      expect(Response.negative(empty_responses)).to eq(0)
    end
  end

  context 'average' do
    it 'finds the average of the answers' do
      expect(Response.average(responses)).to eq(2.5)
    end

    it 'works with no responses' do
      expect(Response.average(empty_responses).nan?).to eq(true)
    end
  end
end
